const express = require('express');
const path = require('path');

const app = express();

var bodyParser = require('body-parser')
app.use( bodyParser.json() ); 

// Database connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/educ');
const db = mongoose.connection;

// Models
const UserModel = require('./models/user')


db.on('error', console.error.bind(console, 'database connection error:'));
db.once('open', function() {

	app.use(express.static('dist'));

	app.post('/api/sign_up', (req, res) => {
		const newUserObj = req.body.user;
		const newUser = UserModel(newUserObj)
		newUser.save(function (err) {
		  if (err) console.log(err);
			res.send({session_id: newUser._id})
		});
	})

	app.listen(8080, () => console.log('Listening on port 8080!'));


});