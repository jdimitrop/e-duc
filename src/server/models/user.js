const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserModelSchema = new Schema({
  name: String,
  surname: String,
  job_title: String,
  speciality: String,
  bio: String,
  age: Number,
  password: String, // fix
  email: String // fix
});

module.exports = mongoose.model('UserModel', UserModelSchema );