const initialState = {
  counter: 1
}


export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case 'INCREASE_COUNTER':
      return Object.assign({}, state, {
        counter: state.counter + 1,
      });

    case 'DECREASE_COUNTER':
      return Object.assign({}, state, {
        counter: state.counter - 1,
      });

    default:
      return state
    }
}