import React, { Component } from 'react';
import { Link } from 'react-router-dom'


export default class Header extends Component {

	render() {
		return (
			<header>
				<div className='wrapper'>
					<div className='logo'>
						e-DUC
					</div>
					<div className='header-panel'>

						<Link to={'/users/1'} className="username">
							Giannis Dim
						</Link>
						<Link to={'/messages'} className="username">
							<i className="fas fa-envelope"></i>
						</Link>
					</div>
				</div>
			</header>
		)
	}
}