import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import GroupPageSideBar from './GroupPageSideBar';

export default class GroupPage extends Component {
  
	render() {

		return (
			<div className='wrapper group-page'>
				<GroupPageSideBar />
				<div className='group-page-main'>
				</div>
			</div>
		)
	}
}