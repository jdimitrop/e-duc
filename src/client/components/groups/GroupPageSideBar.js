import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class GroupPageSideBar extends Component {
  
	render() {

		return (
			<div className="group-page-sidebar">
				<img src='http://via.placeholder.com/260x340'/>
				<ul>
					<li>
						<i class="fas fa-home"></i>
						<span>
							Αρχική
						</span>
					</li>
					<li>
						<i class="fas fa-prescription-bottle"></i>
						<span>
							Περιγραφή
						</span>
					</li>
					<li>
						<i class="fas fa-users"></i>
						<span>
							Μέλη
						</span>
					</li>
				</ul>
			</div>
		)
	}
}


var {a, v} = {a: 1, v: 2}