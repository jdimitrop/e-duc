import React, { Component } from 'react';

export default class ArticlePage extends Component {

  render() {
    return (
      <div className='wrapper article-page'>
        <div class="article-cover">
          <picture>
            <source media="(max-width: 768px)" srcset="http://via.placeholder.com/768x200" />
            <source media="(max-width: 1366px)" srcset="http://via.placeholder.com/1366x350" />
            <img src="http://via.placeholder.com/1920x400" />
          </picture>
          <div className="wrapper-768">
            <div className='user-photo'>
              <img src="http://via.placeholder.com/150x150"/>
            </div>
            <div class="article-content">
              <h1 class="article-title">
                Lorem Ipsum Dolors Lorem Ipsum
              </h1>
              <p>
                Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum
              </p>
              <p>
                Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum
              </p>
              <p>
                Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum
              </p>
              <p>
                Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum Lorem Ipsum Dolors Lorem IpsumLorem 
                Ipsum Dolors Lorem IpsumLorem Ipsum 
                Dolors Lorem IpsumLorem Ipsum Dolors 
                Lorem Ipsum
              </p>
            </div>
            
          </div>
        </div>
      </div>
    );
  }
}
