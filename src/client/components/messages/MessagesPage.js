import React, { Component } from 'react';

export default class MessagesPage extends Component {

  _renderMessagesSideBar() {
    return (
        <div className='messages-sidebar'>
          <h3>
            Messages
          </h3>
          <div className='message-preview cf'>
            <div className='avatar'>
              <img src='http://via.placeholder.com/50x50' />
            </div>
            <div className='message-content'>
              <div className='sender'>
                Giannis Dim
              </div>
              <div className='message-text'>
                Lorem Ipsum Dolor Dolor Dolor
              </div>
            </div>
          </div>
        </div>
    );
  }

  _renderMessages() {
    return(
      <div className='messages-box'>
        <div className='message-preview cf'>
          <div className='avatar'>
            <img src='http://via.placeholder.com/50x50' />
          </div>
          <div className='message-content'>
            <div className='sender'>
              Giannis Dim
            </div>
            <div className='message-text'>
              Lorem Ipsum Dolor Dolor Dolor
            </div>
          </div>
        </div>

        <input type='text' placeholder='Νέο μήνυμα' />
      </div>
    );   
  }

  render() {
    return (
      <div className='wrapper messages-page'>
        {this._renderMessagesSideBar()}
        {this._renderMessages()}
      </div>
    );
  }
}
