import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class UserProfileTab extends Component {
  
	renderTabContent() {
		switch (this.props.activeTabId) {
		  case 'userInfo':
		  	return this.renderUserInfoTab();
		    break;
		  case 'userBio':
		  	return this.renderUserBioTab();
		    break;
		  case 'userBlog':
		  	return this.renderUserBlogTab();
		    break;
		  default:
		  	return this.renderUserInfoTab();
		}
	}

	renderUserBioTab() {
		return (
			<div className="bio-tab">
				Lorem Ipsum is simply dummy text of the 
				printing and typesetting industry. Lorem Ipsum 
				has been the industry's standard dummy text ever 
				since the 1500s, when an unknown printer took a 
				galley of type and scrambled it to make a type 
				specimen book. It has survived not only five centuries, 
				but also the leap into electronic typesetting, remaining
				essentially unchanged. It was popularised in the 
				1960s with the release of Letraset sheets containing.
			</div>
		)
	}

	renderUserBlogTab() {
		return (
			<ul className="blog-tab">
				<li>
					<a href="#">
						<span className="description">
							Lorem Ipsum is simply dummy text of the 
							printing and typesetting industry. Lorem Ipsum 
							has been the industry's standard dummy text ever 
							since the 1500s, when an unknown printer took a 
							galley of type and scrambled it to make a type...	
						</span>
						<Link to={'/articles/1'} className="read-more">
							Διαβάστε το άρθρο
						</Link>
					</a>
				</li>
				<li>
					<a href="#">
						<span className="description">
							Lorem Ipsum is simply dummy text of the 
							printing and typesetting industry. Lorem Ipsum 
							has been the industry's standard dummy text ever 
							since the 1500s, when an unknown printer took a 
							galley of type and scrambled it to make a type...	
						</span>
						<span className="read-more">
							Διβάστε το άρθρο
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span className="description">
							Lorem Ipsum is simply dummy text of the 
							printing and typesetting industry. Lorem Ipsum 
							has been the industry's standard dummy text ever 
							since the 1500s, when an unknown printer took a 
							galley of type and scrambled it to make a type...	
						</span>
						<span className="read-more">
							Διβάστε το άρθρο
						</span>
					</a>
				</li>
			</ul>
		)
	}

	renderUserInfoTab() {
		return(
			<div className="profile-sidebar">
	      <dl className="user-description-list">
	        <dt>Όνομα:</dt>
	        <dd>Γιαννης</dd>
	        <dt>Επίθετο:</dt>
	        <dd>Δημητρόπουλος</dd>

	        <dt>Ηλικεία:</dt>
	        <dd>25</dd>
	        <dt>Ιδικότητα</dt>
	        <dd>Φοιτητής Πληροφορικής</dd>

	        <dt>Εξειδίκευση</dt>
	        <dd>Web Development</dd>
	        <dt>Ενδιαφέροντα</dt>
	        <dd>Γυμναστηρίο, Γκομενάκια, Ουζα</dd>
	      </dl>
			</div>
		)
	}

	render() {
		return (
			<div className="tab-content">
				{this.renderTabContent()}
			</div>
		)
	}
}