import React, { Component } from 'react';
import ProfileSideBar from './ProfileSideBar';
import ProfileMain from './ProfileMain';


export default class UserProfile extends Component {
  
	render() {

	  let params = this.props.match.params

		return (
			<div className="wrapper">
				<ProfileSideBar />
				<ProfileMain />
			</div>
		)
	}
}