import React, { Component } from 'react';
import UserProfileTab from './UserProfileTab';

export default class ProfileMain extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      activeTabId: 'userInfo',
    };
  }

  renderTabsList() {
    const tabs = [
      {
        tabId: 'userInfo',
        tabText: 'Πληροφορίες'
      },
      {
        tabId: 'userBio',
        tabText: 'Βιογραφικό'
      },
      {
        tabId: 'userBlog',
        tabText: 'Αρθρογραφία'
      }
    ];

  	const tabsElements = tabs.map((tab, idx) => {
  		return (<li className={tab.tabId === this.state.activeTabId ? 'active' : ''} 
                  key={idx} 
                  onClick={() => this.setState({activeTabId: tab.tabId})}>
                    {tab.tabText} 
              </li>);
  	})

  	return(
  		<ul className="tabs-list">
  			{ tabsElements }
  		</ul>
  	);
  }

  renderActiveTab() {
  	return(
      <div className="tab-content">
        {this.renderUserInfoTab()}
      </div>
    ); 
  }

	render() {

		return (
			<div className="profile-main">
        <div className="username">
          Giannis Dimitropoulos
        </div>
        <div className="tabs">
          {this.renderTabsList()}
          <UserProfileTab activeTabId={this.state.activeTabId}/>
        </div>
			</div>
		)
	}
}