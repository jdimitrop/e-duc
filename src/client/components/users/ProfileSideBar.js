import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class ProfileSideBar extends Component {
  
	render() {

		return (
			<div className='profile-sidebar'>
				<img src='http://via.placeholder.com/260x340' />
				<ul className='profile-sidebar-actions'>
					<li>
						<Link to={'/messages'} className="read-more">
							<i class="fas fa-envelope"></i>
							<span>
								Αποστολη μηνύματος
							</span>
						</Link>
					</li>
				</ul>
			</div>
		)
	}
}