import React, { Component } from 'react';

import { connect } from "react-redux";
import * as actions from "../../actions";

import axios from 'axios';

class RegistrationPage extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      surname: null,
      job_title: null,
      speciality: null,
      bio: null,
      password: null,
      email: null,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('/api/sign_up', {user: this.state})
    .then((response) => {
      this.userLoggedIn(response.data.session_id);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  userLoggedIn(session_id) {
    alert(session_id)
    this.setCookie('session_id', session_id, 10 )
  }


  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

	render() {
		return (
			<div className="registration-page">
        Counter: {this.props.counter}
				<h3> Registration Page </h3>
        <button onClick={this.props.onIncreaseClick}>
          Add
        </button>
        <button onClick={this.props.onDecreaseClick}>
          Decrease
        </button>
				<form onSubmit={this.handleSubmit}>
					<input type='text' name='name' onChange={this.handleInputChange} placeholder='Όνομα'/>
					<input type='text' name='surname' onChange={this.handleInputChange} placeholder='Επίθετο' />
					<input type='text' name='job_title' onChange={this.handleInputChange} placeholder='Ιδικότητα' />
					<input type='text' name='speciality' onChange={this.handleInputChange} placeholder='Εξειδίκευση' />
					<input type='text' name='bio' onChange={this.handleInputChange} placeholder='Βιογραφικό' />
					<input type='text' name='age' onChange={this.handleInputChange} placeholder='Ηλικεία' />
					<input type='text' name='password' onChange={this.handleInputChange} placeholder='Κωδικός' />
					<input type='text' name='email' onChange={this.handleInputChange} placeholder='Email' />
					<input type='submit' value='Αποστολή' placeholder='Όνομα' />
				</form>
			</div>
		)
	}
}

const mapStateToProps = state => {
  return {
    counter: state.counter
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onIncreaseClick: () => dispatch(actions.increaseCounter()),
    onDecreaseClick: () => dispatch(actions.decreaseCounter()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationPage)
