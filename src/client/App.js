import React, { Component } from 'react';
import { Route } from 'react-router-dom'

import { Provider } from "react-redux";
import store from './store'

import Header from './components/shared/Header';
import UserProfile from './components/users/UserProfile';
import ArticlePage from './components/articles/ArticlePage';
import GroupPage from './components/groups/GroupPage';
import MessagesPage from './components/messages/MessagesPage';
import RegistrationPage from './components/registration/RegistrationPage';


import './stylesheets/shared.scss';
import './stylesheets/header.scss';
import './stylesheets/profile.scss';
import './stylesheets/articles.scss';
import './stylesheets/groups.scss';
import './stylesheets/messages.scss';
import './stylesheets/registration.scss';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { username: null };
  }

  componentDidMount() {
    fetch('/api/getUsername')
      .then(res => res.json())
      .then(user => this.setState({ username: user.username }));
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          <Header />
          <Route path='/users/:userID' component={UserProfile}/>
          <Route path='/articles/:articleID' component={ArticlePage}/>
          <Route path='/groups/:groupID' component={GroupPage}/>
          <Route path='/messages' component={MessagesPage}/>
          <Route path='/sign_up' component={RegistrationPage}/>
        </div>
      </Provider>
    );
  }
}
